FROM alpine:latest

COPY ./weather.py /

RUN apk update && \
    apk add py3-requests \
    py3-flask && \
    chmod +x /weather.py

ENTRYPOINT ["/weather.py"]
