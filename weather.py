#!/usr/bin/python3

from flask import Flask
from flask import request
import requests
import json
import os

APP_PORT = os.environ.get('APP_PORT')
API_KEY = os.environ.get('API_KEY')
LAT = os.environ.get('GEO_LAT')
LON = os.environ.get('GEO_LON')

api_url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric" % (LAT, LON, API_KEY)

def get_weather():
  response = requests.get(api_url)
  data = json.loads(response.text)
  temp = data["current"]["temp"]
  feel = data["current"]["feels_like"]
  pressure = data["current"]["pressure"]
  wind_speed = data["current"]["wind_speed"]

  line = ("""# HELP temperature Temperature value from OpenWeatherMap API
# TYPE temperature gauge
temperature{id=\"temperature\"} %s
# HELP feels_like Feeling Temperature value from OpenWeatherMap API
# TYPE feels_like gauge
feels_like{id=\"feels_like\"} %s
# HELP pressure Pressure value from OpenWeatherMap API
# TYPE pressure gauge
pressure{id=\"pressure\"} %s
# HELP wind_speed Wind Speed value from OpenWeatherMap API
# TYPE wind_speed gauge
wind_speed{id=\"wind_speed\"} %s
""" % (temp, feel, pressure, wind_speed))

  return line

app = Flask(__name__)

@app.route('/metrics')
def index():
  weather = get_weather()

  if not weather:
     print("Houston! We have a problem!")
     exit()
  else:
     return weather

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=APP_PORT)
