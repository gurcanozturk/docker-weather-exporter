# docker-weather-exporter

This docker image is a prometheus exporter to get weather info from OpenWeatherMap with coordinates. It requires api key from https://openweathermap.org/api

## Build it

docker build -t weather-exporter .


## Run it
docker run -d --name weather-exporter -e API_KEY=<WEATHERMAP_API_KEY> -e GEO_LAT=<LATITUDE> -e GEO_LON=<LONGITUDE> -e APP_PORT=<PORT> -p <PORT>:<APP_PORT> weather-exporter


## Check it
```
root@pve # curl localhost:7010/metrics
# HELP temperature Temperature value from OpenWeatherMap API
# TYPE temperature gauge
temperature{id="temperature"} 2.76
# HELP feels_like Feeling Temperature value from OpenWeatherMap API
# TYPE feels_like gauge
feels_like{id="feels_like"} -1.79
# HELP pressure Pressure value from OpenWeatherMap API
# TYPE pressure gauge
pressure{id="pressure"} 1032
# HELP wind_speed Wind Speed value from OpenWeatherMap API
# TYPE wind_speed gauge
wind_speed{id="wind_speed"} 5.66
```
## Graph it
Edit new panel in your Grafana and graph it.